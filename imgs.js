//code imgs

let highwayImg; 
let actorImg;
let carImg;
let carImg2;
let carImg3;

// game's sounds
let soundOfGame;
let soundOfCollide;
let soundOfPoints;

function preload() {
  highwayImg = loadImage("img/estrada.png");
  actorImg = loadImage("img/ator-1.png");
  carImg = loadImage("img/carro-1.png");
  carImg2 = loadImage("img/carro-2.png");
  carImg3 = loadImage("img/carro-3.png");
  carImgs = [carImg, carImg2, carImg3, carImg, carImg2, carImg3];
  
  soundOfGame = loadSound("sounds/trilha.mp3");
  soundOfCollide = loadSound("sounds/colidiu.mp3");
  soundOfPoints = loadSound("sounds/pontos.wav");
}