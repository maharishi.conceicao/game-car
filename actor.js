//actor code
let xActor = 85
let yActor = 366
let collide = false
let myPoints = 0

function showActor(){
  image(actorImg, xActor, yActor, 30, 30);
}

function moveActor(){
  if (keyIsDown(UP_ARROW)){
    yActor -= 3;
  }
   if (keyIsDown(DOWN_ARROW)){
     if(canMove()){
      yActor += 3;
     }
   }
}

function verifyCollide(){
  //collideRectCircle(x1, y1, width1, height1, cx, cy, diameter)
  for (let i = 0; i < carImgs.length; i++){
    collide = collideRectCircle(xCars[i], yCars[i], widthCar, heightCar, xActor, yActor, 15)
    if (collide){
      backActorInitialPosition();
      soundOfCollide.play();
      if (pointsBigerThanZero()){
      myPoints -= 1;
     }
    }
  }
}

function backActorInitialPosition(){
  yActor = 366;
}

function includePoints(){
  textAlign(CENTER);
  textSize(25);
  text(myPoints, width / 5, 27);
  fill(color(255, 240, 60));
}

function makePoints(){
  if (yActor < 15){
    myPoints += 1;
    soundOfPoints.play();
    backActorInitialPosition();
  }
}

function pointsBigerThanZero(){
  return myPoints > 0
}

function canMove(){
  return yActor < 366;
}